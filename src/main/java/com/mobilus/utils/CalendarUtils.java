package com.mobilus.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Fikran on 31 Des 2015.
 */
public class CalendarUtils {
    //format time HH:mm:ss
    public static void setTime(Calendar calendar, String time){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Calendar mCalendar = Calendar.getInstance();
        try {
            Date date = sdf.parse(time);
            mCalendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY, mCalendar.get(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, mCalendar.get(Calendar.MINUTE));
            calendar.set(Calendar.SECOND, mCalendar.get(Calendar.SECOND));
            calendar.set(Calendar.MILLISECOND, 0);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
