package com.mobilus.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;

import com.readystatesoftware.systembartint.SystemBarTintManager;

/**
 * Created by MuhammadDhito on 2/18/2015.
 */
public class WindowCompatUtil {

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarcolor(Window window, int color) {
        int buildVersionSdk = Build.VERSION.SDK_INT;
        switch (Build.VERSION.SDK_INT){
            case Build.VERSION_CODES.LOLLIPOP:
                window.setStatusBarColor(color);
                break;
            case Build.VERSION_CODES.KITKAT:

                break;
            default:
                break;
        }
    }

    public static void initStatusBarColor(Activity activity, View root, int colorResourceId) {
        SystemBarTintManager tintManager = new SystemBarTintManager(activity);
        final int statusBarHeight = getSystemBarHeight(tintManager);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
                && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

            if (root == null) {
                tintManager.setStatusBarTintResource(colorResourceId);
                tintManager.setStatusBarTintEnabled(true);
                root = activity.findViewById(android.R.id.content);
            }
            root.setPadding(0, statusBarHeight, 0, 0);
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && root != null){
            activity.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            root.setPadding(0, statusBarHeight, 0, 0);
        }
    }

    public static void initStatusBarForLollipop(Activity activity, View root) {
        SystemBarTintManager tintManager = new SystemBarTintManager(activity);
        final int statusBarHeight = getSystemBarHeight(tintManager);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && root != null){
            activity.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            root.setPadding(0, statusBarHeight, 0, 0);
        }
    }

    public static int getSystemBarHeight(Activity activity) {
        SystemBarTintManager tintManager = new SystemBarTintManager(activity);
        return getSystemBarHeight(tintManager);
    }

    public static int getSystemBarHeight(SystemBarTintManager tintManager) {
        return tintManager.getConfig().getStatusBarHeight();
    }

    public static int getPixel(Context mContext, int dp) {
        Resources r = mContext.getResources();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                r.getDisplayMetrics()
        );

        return px;
    }
}
