package com.mobilus.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import java.io.File;

/**
 * Created by Fikran on 16 Nov 2015.
 */
public class BitmapUtils {

    public static boolean isImage(String path){
        String tmpPath = path.trim();
        if(TextUtils.isEmpty(tmpPath)){
            return false;
        }
        File f = new File(tmpPath);
        if(!f.isFile()){
            return false;
        }

        return isImage(f);
    }

    public static boolean isImage(File f){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), options);
        if (options.outWidth != -1 && options.outHeight != -1) {
            return true;
        }

        return false;
    }
}
