package com.mobilus.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by MuhammadDhito on 1/30/2015.
 */
public class HashHelper {

    private static final String SALT = "T#U&$t1CWECyBx1!"; // 16 random chars

    public static byte[] getHash(String text, boolean usingSalt) {
        if (usingSalt)
            text += SALT.substring(0,4) + text + SALT.substring(4,8);

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        digest.reset();
        return digest.digest(text.getBytes());
    }

    public static String bin2hex(byte[] data) {
        return String.format("%0" + (data.length*2) + "X", new BigInteger(1, data));
    }
}
