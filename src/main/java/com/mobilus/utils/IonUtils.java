package com.mobilus.utils;

import android.content.Context;
import android.text.TextUtils;

import com.koushikdutta.async.util.FileCache;

import java.io.File;

/**
 * Created by Fikran on 16 Nov 2015.
 */
public class IonUtils {
    private Context context;

    private IonUtils(){}

    public static IonUtils with(Context context){
        IonUtils ionUtils = new IonUtils();
        ionUtils.context = context;

        return ionUtils;
    }

    public boolean isImageCacheReady(String url){
        File file = getFileCache(url);
        if(file == null){
            return false;
        }

        return BitmapUtils.isImage(file);
    }

    public String generatePathCache(String url){
        if(TextUtils.isEmpty(url)){
            return "";
        }
        String key = FileCache.toKeyString(url);

        return context.getCacheDir().getAbsolutePath() + "/ion/" + key + ".1";
    }

    public File generateFileCache(String url){
        if(TextUtils.isEmpty(url)){
            return null;
        }
        String key = FileCache.toKeyString(url);
        File file = new File(context.getCacheDir().getAbsolutePath() + "/ion/" + key + ".1");

        return file;
    }

    public File getFileCache(String url){
        if(TextUtils.isEmpty(url)){
            return null;
        }
        String key = FileCache.toKeyString(url);
        File file = new File(context.getCacheDir().getAbsolutePath()
                + "/ion/" + key + ".1");

        return file.isFile() ? file : null;
    }
}
