package com.mobilus.utils;

import android.content.Intent;

/**
 * Created by MuhammadDhito on 1/28/2015.
 */
public interface ActivityResultFromFragment {

    public void onActivityResultFromFragment(int requestCode, int resultCode, Intent data);
}
