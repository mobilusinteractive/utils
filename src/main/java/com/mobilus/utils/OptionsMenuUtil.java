package com.mobilus.utils;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.MenuItem;

/**
 * Created by Fikran on 26 Okt 2015.
 */
public class OptionsMenuUtil {

    public static void setColorTintOnMenuItem(Context context, MenuItem item, int colorResourceId){
        Drawable newIcon = item.getIcon();
        if(newIcon == null){
            return;
        }
        newIcon.mutate().setColorFilter(context.getResources().getColor(colorResourceId), PorterDuff.Mode.SRC_IN);
        item.setIcon(newIcon);
    }
}
