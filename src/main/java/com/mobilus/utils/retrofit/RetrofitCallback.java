package com.mobilus.utils.retrofit;

import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.mobilus.utils.GsonUtils;
import com.mobilus.utils.LogUtils;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Fikran on 03 Mei 2016.
 */
public abstract class RetrofitCallback implements Callback<ResponseBody>{
    private static String TAG = RetrofitCallback.class.getSimpleName();

    public abstract void onResponse(int status, JsonObject object);

    protected String getTag(){
        return TAG;
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        ResponseBody body = response.body();
        ResponseBody errorBody = response.errorBody();
        String stringBody = null;
        String stringErrorBody = null;
        try {
            if(body != null){
                stringBody = body.string();
            }
            if(errorBody != null){
                stringErrorBody = errorBody.string();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        LogUtils.i(getTag(), "body > " + stringBody);
        LogUtils.i(getTag(), "message > " + response.message());
        LogUtils.i(getTag(), "code > " + response.code());
        LogUtils.i(getTag(), "isSuccessful > " + response.isSuccessful());
        LogUtils.i(getTag(), "isSuccessful > " + response.isSuccessful());
        LogUtils.i(getTag(), "errorBody > " + stringErrorBody);
        LogUtils.i(getTag(), "headers > " + response.headers());
        LogUtils.i(getTag(), "raw > " + response.raw());

        if(stringBody == null){
            onResponse(0, null);
            return;
        }

        JsonObject object = null;
        int status = 0;

        try {
            object = (JsonObject) GsonUtils.getParser().parse(stringBody);
            status = object.has("status") ? object.get("status").getAsInt() : 0;
        }catch (JsonSyntaxException e){
            e.printStackTrace();
            LogUtils.i(getTag(), "JsonSyntaxException > " + e.getMessage());
        }catch (JsonIOException e){
            e.printStackTrace();
            LogUtils.i(getTag(), "JsonIOException > " + e.getMessage());
        }catch (Exception e){
            e.printStackTrace();
            LogUtils.i(getTag(), "Exception > " + e.getMessage());
        }

        onResponse(status, object);
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        LogUtils.i(getTag(), "onFailure > " + t.getMessage());
		onResponse(0, null);
    }

    public static JsonObject getJsonObjectCallback(Response<ResponseBody> response){
        return getJsonObjectCallback(response, TAG);
    }

    public static JsonObject getJsonObjectCallback(Response<ResponseBody> response, final String tag){
        if(response == null){
            return null;
        }
        final JsonObject[] jsonObjectCallbacks = new JsonObject[1];
        RetrofitCallback callback = new RetrofitCallback() {
            @Override
            public void onResponse(int status, JsonObject object) {
                jsonObjectCallbacks[0] = object;
            }

            @Override
            protected String getTag() {
                return tag;
            }
        };

        callback.onResponse(null, response);

        return jsonObjectCallbacks[0];
    }
}
