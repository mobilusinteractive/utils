package com.mobilus.utils;

import android.content.Context;
import android.text.TextUtils;

import java.io.File;

/**
 * Created by MuhammadDhito on 5/22/2015.
 */
public class DirectoryUtil {

    public static String getNoteImageDir(Context context, Long noteId){
        String dir = context.getExternalFilesDir(null).getAbsolutePath() + "/note/";
//        String dir = context.getExternalFilesDir(null).getAbsolutePath() + "/note/" + (noteId != null ? noteId + "/" : "");
//        String dir = context.getCacheDir().getAbsolutePath() + "/note/" + (noteId != null ? noteId + "/" : "");

        File imgTempDir = new File(dir);

        // Check if directory already exists
        if (!imgTempDir.isDirectory()) {
            imgTempDir.mkdirs();
        }
        return dir;
    }
}
