package com.mobilus.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

/**
 * Created by Fikran on 01 Des 2015.
 */
public class NotificationUtils {
    private Context context;
    private int id = 34;
    private NotificationManager mNotificationManager;
    private String tag = null;

    private Integer smallIconResourceId;
    private Integer largeIconResourceId;

    private NotificationUtils(){}

    public static NotificationUtils getInstance(Context context, int smallIconResourceId, int largeIconResourceId){
        NotificationUtils notificationUtils = new NotificationUtils();
        notificationUtils.context = context;
        notificationUtils.mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationUtils.setSmallIcon(smallIconResourceId);
        notificationUtils.setLargeIcon(largeIconResourceId);

        return notificationUtils;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setSmallIcon(int smallIconResourceId) {
        this.smallIconResourceId = smallIconResourceId;
    }

    public void setLargeIcon(int largeIconResourceId) {
        this.largeIconResourceId = largeIconResourceId;
    }

    public void notify(int rTitle, int rMessage, Intent notificationIntent){
        String title = "" + context.getString(rTitle);
        String message = "" + context.getString(rMessage);

        notify(title, message, notificationIntent);
    }

    public void notify(String title, String message, Intent notificationIntent) {
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent resultPendingIntent
                = PendingIntent.getActivity(context, 0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(),
                largeIconResourceId);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(smallIconResourceId)
                        .setLargeIcon(largeIcon)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setDefaults(-1)
                        .setAutoCancel(true)
                        .setContentIntent(resultPendingIntent);
        mNotificationManager.notify(tag, id, mBuilder.build());
    }

    public static NotificationManager getNotificationManager(Context context) {
        return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void cancel(int id){
        mNotificationManager.cancel(id);
    }

    public void cancel(String tag, int id){
        mNotificationManager.cancel(tag, id);
    }

    public void cancelAll(){
        mNotificationManager.cancelAll();
    }
}
