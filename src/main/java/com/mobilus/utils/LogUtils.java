package com.mobilus.utils;

import android.util.Log;
import java.util.Scanner;

/**
 * Created by Fikran on 16 Des 2015.
 */
public class LogUtils {

    public static void i(String tag, String msg){
        Scanner scanner = new Scanner("" + msg);
        while (scanner.hasNextLine()) {
            String row = scanner.nextLine();
            Log.i(tag, "> " + row);
        }
        scanner.close();
    }

    public static void d(String tag, String msg){
        Scanner scanner = new Scanner("" + msg);
        while (scanner.hasNextLine()) {
            String row = scanner.nextLine();
            Log.d(tag, "> " + row);
        }
        scanner.close();
    }
}