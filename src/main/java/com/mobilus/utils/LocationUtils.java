package com.mobilus.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;


/**
 * Created by Fikran on 25 Nov 2015.
 */
public class LocationUtils {
    private static String TAG = LocationUtils.class.getSimpleName();

    public static int ACTIVITY_LOCATION_SOURCE_SETTINGS = 8863;

    Activity activity;

    private LocationUtils(){}

    public static LocationUtils getInstance(Activity activity){
        LocationUtils locationUtils = new LocationUtils();
        locationUtils.activity = activity;

        return locationUtils;
    }

    public boolean isAvailable() {
        LocationManager locationManager;
        locationManager = (LocationManager) activity
                .getSystemService(Context.LOCATION_SERVICE);

        boolean isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (isGPSEnabled || isNetworkEnabled) {
            return true;
        }

        Log.i(TAG, "no network");

        return false;
    }

    public void checkLocationService(){
        if(!isAvailable()){
            showAlertLocationServices();
        }
    }

    private void showAlertLocationServices() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(activity.getString(R.string.location_service_is_turned_off_please_turn_on));
        builder.setPositiveButton(activity.getString(R.string.accept), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivityForResult(intent, ACTIVITY_LOCATION_SOURCE_SETTINGS);
            }
        });
        builder.setNegativeButton(activity.getString(R.string.cancel), null);
        builder.show();
    }
}
