package com.mobilus.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.widget.RatingBar;

/**
 * Created by Fikran on 06 Jun 2016.
 */
public class RatingBarUtils {
    public static void init(Context context, RatingBar ratingBar, int colorResourceId){
        Drawable progress = ratingBar.getProgressDrawable();
        DrawableCompat.setTint(progress, context.getResources().getColor(colorResourceId));
    }
}
