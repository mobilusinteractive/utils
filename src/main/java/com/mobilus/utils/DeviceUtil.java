package com.mobilus.utils;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

/**
 * Created by MuhammadDhito on 6/3/2015.
 */
public class DeviceUtil {

    /**
     * Generate device ID which will be used to identify a device.
     * Device ID defined here is a combination of MAC Address and telephony device ID
     *
     * @return device ID of the device
     */
    public static String getDeviceId(Context context) {
        String deviceId = "";

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        String wlanMacAdd = wifiManager.getConnectionInfo().getMacAddress();

        try {
            deviceId = wlanMacAdd.replace(":", "");
        } catch (NullPointerException e2) {
        }

        try {
            TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (!TextUtils.isEmpty(mTelephony.getDeviceId())){
                deviceId += mTelephony.getDeviceId();
            }
        }catch (SecurityException se){}

        if (!TextUtils.isEmpty(deviceId)){
            deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }

        return deviceId;
    }
}
