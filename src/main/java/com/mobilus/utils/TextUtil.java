package com.mobilus.utils;


import android.content.Context;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.UUID;

public class TextUtil {
	
	public static String URLencode(String words){
		String encodedString = null;
		
		try {
			encodedString = URLEncoder.encode(words, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			encodedString = words;
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return encodedString;
	}
	
	public static String arrayToText(List<String> arr) {
		String text = "";
		
		if (arr.size() > 0) {
			text += arr.get(0);
		}
		
		for (int i = 1; i < arr.size(); i++) {
			text += ", " + arr.get(i);
		}
		
		return text;
	}
	
	public static String arrayToText(List<String> arr, boolean removeSubstring) {
		String text = "";
		
		if (arr.size() > 0) {
			text += arr.get(0);
		}
		
		for (int i = 1; i < arr.size(); i++) {
			if (removeSubstring) {
				if (!text.contains(arr.get(i)))
					text += ", " + arr.get(i);
			} else
				text += ", " + arr.get(i);
		}
		
		return text;
	}
	
	// Correct Typo
	public static String correctEmailTypo(String text) {
		return text.replace("mai|", "mail");
	}
	
	/**
	 * Replace every commas within a word 
	 * 
	 * @param text to clean
	 * @return 
	 */
	public static String correctCommas(String text) {
		return text.replaceAll("(\\S)(\\,)(\\S)", "$1.$3");
	}
	
	public static String generateRandomString() {
		String randString = UUID.randomUUID().toString();
		randString = randString.replace("-", "");
		String currentTimeMillis = System.currentTimeMillis() + "";
		try {
			randString = randString.substring(currentTimeMillis.length());
			randString = currentTimeMillis + "n" + randString;
		} catch (IndexOutOfBoundsException e) {
			randString = currentTimeMillis;
		}
		
		return randString;
	}
}