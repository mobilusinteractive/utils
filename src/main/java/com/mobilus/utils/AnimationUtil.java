package com.mobilus.utils;

import android.view.View;
import android.view.animation.AnimationUtils;

/**
 * Created by Fikran on 24 Nov 2015.
 */
public class AnimationUtil {
    public static void slideUp(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(view.getContext(),
                R.anim.slide_up));
    }

    public static void slideDown(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(view.getContext(),
                R.anim.slide_down));
    }
}
