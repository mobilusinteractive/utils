package com.mobilus.utils;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Fikran on 22 Apr 2016.
 */
public class InputMethodManagerUtils {
    public static void hideSoftKeyboard(Activity activity) {
        try{
            InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }catch (NullPointerException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
