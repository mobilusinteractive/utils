package com.mobilus.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileManagement {

    private int quality = 100;

    public FileManagement() {

    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public String imageToString(String imagePath) {
        String result = "";
        if (isFileJpgOrPng(imagePath)) {
            Bitmap.CompressFormat bitmapCompressFormat = Bitmap.CompressFormat.JPEG;
            String imageExt = getFileExtension(imagePath);
            if (imageExt.equalsIgnoreCase("PNG")) {
                bitmapCompressFormat = Bitmap.CompressFormat.PNG;
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            FileInputStream iStream = null;
            try {
                iStream = new FileInputStream(imagePath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            BitmapFactory.Options preOptions = new BitmapFactory.Options();
            preOptions.inJustDecodeBounds = true;

            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, preOptions);
            BitmapFactory.Options options = null;
            if (preOptions.outWidth * preOptions.outHeight > 1300000) {
                options = new BitmapFactory.Options();
                options.inSampleSize = 2;
            }
            bitmap = BitmapFactory.decodeStream(iStream, null, options);
            bitmap.compress(bitmapCompressFormat, 100, baos);
            byte[] b = baos.toByteArray();
            result = Base64.encodeToString(b, Base64.DEFAULT);
        }

        return result;
    }

    public String imageToStringFixRotating(String imagePath,
                                           int maxHeight, int maxWidth) {
        String result = "";
        if (isFileJpgOrPng(imagePath)) {
            Bitmap.CompressFormat bitmapCompressFormat = Bitmap.CompressFormat.JPEG;
            String imageExt = getFileExtension(imagePath);
            if (imageExt.equalsIgnoreCase("PNG")) {
                bitmapCompressFormat = Bitmap.CompressFormat.PNG;
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            FileInputStream iStream = null;
            try {
                iStream = new FileInputStream(imagePath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            BitmapFactory.Options preOptions = new BitmapFactory.Options();
            preOptions.inJustDecodeBounds = true;

            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, preOptions);
            BitmapFactory.Options options = null;
            int inSampleSize = 1;
            if (preOptions.outWidth > preOptions.outHeight) {
                while ((preOptions.outWidth / inSampleSize) > maxWidth) {
//					inSampleSize++;
                    inSampleSize *= 2;
                }
            } else {
                while ((preOptions.outHeight / inSampleSize) > maxHeight) {
//					inSampleSize++;
                    inSampleSize *= 2;
                }
            }
            /*
			if (inSampleSize != 1) {
				if (inSampleSize % 2 != 0) { // odd
					inSampleSize++;
				}
			}*/
			
			/*
			int width_tmp=bitmap.getWidth(), height_tmp=bitmap.getHeight();
	        while(true){
	            if(width_tmp/2<maxHeight || height_tmp/2<maxWidth)
	                break;
	            width_tmp/=2;
	            height_tmp/=2;
	            inSampleSize*=2;
	        }
			*/
            options = new BitmapFactory.Options();
            options.inSampleSize = inSampleSize;
            bitmap = BitmapFactory.decodeStream(iStream, null, options);

            try {
                ExifInterface exif = new ExifInterface(imagePath);
                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION, 1);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                }
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                        bitmap.getHeight(), matrix, true); // rotating bitmap
            } catch (Exception e) {

            }

            bitmap.compress(bitmapCompressFormat, 100, baos);
            byte[] b = baos.toByteArray();
            result = Base64.encodeToString(b, Base64.DEFAULT);
        }

        return result;
    }

    public String imageToStringWithoutSampling(String imagePath) {
        String result = "";
        if (isFileJpgOrPng(imagePath)) {
            Bitmap.CompressFormat bitmapCompressFormat = Bitmap.CompressFormat.JPEG;
            String imageExt = getFileExtension(imagePath);
            if (imageExt.equalsIgnoreCase("PNG")) {
                bitmapCompressFormat = Bitmap.CompressFormat.PNG;
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            FileInputStream iStream = null;
            try {
                iStream = new FileInputStream(imagePath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Bitmap bitmap = BitmapFactory.decodeStream(iStream, null, null);
            bitmap.compress(bitmapCompressFormat, 100, baos);
            byte[] b = baos.toByteArray();
            result = Base64.encodeToString(b, Base64.DEFAULT);
        }

        return result;
    }

    public String getFileExtension(String filePath) {
        String result = "";
        File file = new File(filePath);
        String imageName = file.getName();
        try {
            result = imageName.substring(imageName.lastIndexOf(".") + 1,
                    imageName.length());
        } catch (Exception e) {
        }

        return result;
    }

    public boolean isFileJpgOrPng(String filePath) {
        boolean result = false;
        String fileExt = getFileExtension(filePath);
        if (fileExt.equalsIgnoreCase("JPEG") || fileExt.equalsIgnoreCase("JPG")
                || fileExt.equalsIgnoreCase("PNG")) {
            result = true;
        }
        return result;
    }

    public String fileToString(File file) {
        String result = null;
        DataInputStream in = null;

        try {
            // File f = new File(file);
            byte[] buffer = new byte[(int) file.length()];
            in = new DataInputStream(new FileInputStream(file));
            in.readFully(buffer);
            // result = new String(buffer);
            result = Base64.encodeToString(buffer, Base64.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException("IO problem in fileToString", e);
        } finally {
            try {
                in.close();
            } catch (IOException e) { /* ignore it */
            }
        }
        return result;
    }

    public Bitmap scaleBitmap(Bitmap bm, int maxWidth, int maxHeight) {
        Bitmap scaledBitmap = bm;
        try {
            int currentWidth = bm.getWidth();
            int currentHeight = bm.getHeight();
            int newWidth = maxWidth;
            int newHeight = maxHeight;

            if ((currentWidth > maxWidth) || (currentHeight > maxHeight)) {
                if ((currentWidth / currentHeight) < (maxWidth / maxHeight)) {
                    newHeight = maxHeight;
                    float newWidthfloat = (currentWidth * maxHeight)
                            / currentHeight;
                    newWidth = Math.round(newWidthfloat);
                } else if ((currentWidth / currentHeight) > (maxWidth / maxHeight)) {
                    newWidth = maxWidth;
                    float newHeightfloat = (currentHeight * maxWidth)
                            / currentWidth;
                    newHeight = Math.round(newHeightfloat);
                } else {
                    newWidth = currentWidth;
                    newHeight = currentHeight;
                }
                scaledBitmap = Bitmap.createScaledBitmap(bm, newWidth,
                        newHeight, true);
            } else {
                scaledBitmap = bm;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return scaledBitmap;
    }

    public Bitmap pathToScaledBitmap(String path, int maxWidth,
                                     int maxHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        FileInputStream iStream = null;
        try {
            iStream = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(iStream, null, options);
        bitmap = scaleBitmap(bitmap, maxWidth, maxHeight);
        return bitmap;
    }

    public void bitmapToSavedFile(Bitmap bitmap, File image) {
        bitmapToSavedFile(bitmap, image.getParent(), image.getName());
    }

    public void bitmapToSavedFile(Bitmap bitmap, String path,
                                  String fileName) {
        bitmapToSavedFile(bitmap, path, fileName, quality);
    }

    public void bitmapToSavedFile(Bitmap bitmap, String path,
                                  String fileName, int quality) {
        if (isFileJpgOrPng(fileName)) {
            Bitmap.CompressFormat bitmapCompressFormat = Bitmap.CompressFormat.JPEG;
            String imageExt = getFileExtension(fileName);
            if (imageExt.equalsIgnoreCase("PNG")) {
                bitmapCompressFormat = Bitmap.CompressFormat.PNG;
            }

            FileOutputStream fOut = null;

            File folder = new File(path);
            if (!folder.exists()) {
                folder.mkdirs();
            }

            File file = new File(path, fileName);
            try {
                fOut = new FileOutputStream(file);
                bitmap.compress(bitmapCompressFormat, quality, fOut);
                bitmap.recycle();
                fOut.flush();
                fOut.close();
            } catch (Exception e) {
            }
        } else {
            Bitmap.CompressFormat bitmapCompressFormat = Bitmap.CompressFormat.JPEG;

            FileOutputStream fOut = null;

            File folder = new File(path);
            if (!folder.exists()) {
                folder.mkdirs();
            }

            File file = new File(path, fileName);
            try {
                fOut = new FileOutputStream(file);
                bitmap.compress(bitmapCompressFormat, quality, fOut);
//				bitmap.recycle();
                fOut.flush();
                fOut.close();
            } catch (Exception e) {
            }
        }
    }

    public Bitmap getBitmap(String path, int maxHeight, int maxWidth) {
        InputStream iStream = null;
        try {
            iStream = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        BitmapFactory.Options preOptions = new BitmapFactory.Options();
        preOptions.inJustDecodeBounds = true;
//		
        Bitmap bitmap = BitmapFactory.decodeFile(path, preOptions);

        // ////////////////
        // int pixel = bitmap.getWidth()*bitmap.getHeight();
        // if(pixel > 1300000){
        // options = new BitmapFactory.Options();
        // options.inSampleSize = 2;
        // }
        // ////////////////
        int inSampleSize = 1;/*
		if (bitmap.getWidth() > bitmap.getHeight()) {
			while ((bitmap.getWidth() / inSampleSize) > maxWidth) {
				inSampleSize++;
			}
		} else {
			while ((bitmap.getHeight() / inSampleSize) > maxHeight) {
				inSampleSize++;
			}
		}*/


        int width_tmp = preOptions.outWidth;
        int height_tmp = preOptions.outHeight;

        while (width_tmp >= maxHeight || height_tmp >= maxWidth) {
            width_tmp /= 2;
            height_tmp /= 2;
            inSampleSize *= 2;
        }

        BitmapFactory.Options options = null;
        options = new BitmapFactory.Options();
        options.inMutable = true;
        options.inSampleSize = inSampleSize;

//		options.inJustDecodeBounds = false;

        // test these options
//		options.inJustDecodeBounds = false;
//		options.inPreferredConfig = Config.RGB_565;
//		options.inDither = true;

        bitmap = BitmapFactory.decodeStream(iStream, null, options);

        try {
            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 1);
            boolean rotate = false;
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                rotate = true;
            } else if (orientation == 3) {
                matrix.postRotate(180);
                rotate = true;
            } else if (orientation == 8) {
                matrix.postRotate(270);
                rotate = true;
            }

            if (rotate) {
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                        bitmap.getHeight(), matrix, true); // rotating bitmap
            }
        } catch (Exception e) {

        }

//		Log.i("PERF", "imageDim: " + preOptions.outWidth + "x" + preOptions.outHeight + " --> " + width_tmp + "x" + height_tmp);

        try {
            iStream.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return bitmap;
    }

    public static void deleteRecursive(String path) {
        File fileOrDirectory = new File(path);
        if (fileOrDirectory.exists()) {
            if (fileOrDirectory.isDirectory())
                for (File child : fileOrDirectory.listFiles()) {
                    child.delete();
                    deleteRecursive(child.getAbsolutePath());
                }

            fileOrDirectory.delete();
        }
    }

    public String getAssetPath(Context context, String filename) {
        Log.i("filename", filename);
        File f = new File(context.getCacheDir() + "/" + filename);
        if (!f.exists())
            try {
                AssetManager assetManager = context.getAssets();
                InputStream is = assetManager.open(filename);
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                FileOutputStream fos = new FileOutputStream(f);
                fos.write(buffer);
                fos.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        return f.getPath();
    }

    public Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        // Compute the scaling factors to fit the new height and width, respectively.
        // To cover the final image, the final scaling will be the bigger 
        // of these two.
        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        // Now get the size of the source bitmap when scaled
        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        // Let's find out the upper left coordinates if the scaled bitmap
        // should be centered in the new size give by the parameters
        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        // The target rectangle for the new, scaled version of the source bitmap will now
        // be
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

        // Finally, we create a new bitmap of the specified size and draw our new,
        // scaled bitmap onto it.
        Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        Canvas canvas = new Canvas(dest);
        canvas.drawBitmap(source, null, targetRect, null);

        return dest;
    }

    public void copyImage(String srcFileName, String dstFileName, int maxFrameSize) {
//    	Bitmap srcBitmap = getBitmap(srcFileName, 1600, 1600);
        Bitmap srcBitmap = getBitmap(srcFileName, maxFrameSize, maxFrameSize);

        int srcHeight = srcBitmap.getHeight();
        int srcWidth = srcBitmap.getWidth();

        int dstHeight = maxFrameSize;
        int dstWidth = maxFrameSize;

        if (srcHeight > srcWidth) {
            dstWidth = (int) maxFrameSize * srcWidth / srcHeight;
        } else if (srcHeight < srcWidth) {
            dstHeight = (int) maxFrameSize * srcHeight / srcWidth;
        }

//    	Bitmap destBitmap = Bitmap.createScaledBitmap(srcBitmap, dstWidth, dstHeight, true);
        File dstFile = new File(dstFileName);
        if (!(new File(dstFile.getParent())).exists()) {
            (new File(dstFile.getParent())).mkdirs();
        }
        bitmapToSavedFile(srcBitmap, dstFile.getParent(), dstFile.getName());

        srcBitmap.recycle();
//        destBitmap.recycle();
    }
}
