package com.mobilus.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v4.app.FragmentActivity;

/**
 * Created by Fikran on 25 Nov 2015.
 */
public class DialogUtils {
    public static ProgressDialog createProgressDialog(FragmentActivity activity, String title){
        ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setMessage(title);

        return dialog;
    }

    public static ProgressDialog createProgressDialog(Activity activity, String title){
        ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setMessage(title);

        return dialog;
    }
}
